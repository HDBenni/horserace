var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/client/index.html');
});

http.listen(12000, function () {
    console.log('listening on *:12000');
});


var allPlayer = [];
var rooms = [{NAME: "ROOM1", PLAYERS: 0, P: [], TURN: null, STATUS: "JOINABLE", MAP: null},
    {NAME: "ROOM2", PLAYERS: 0, P: [], TURN: null, STATUS: "JOINABLE", MAP: null},
    {NAME: "ROOM3", PLAYERS: 0, P: [], TURN: null, STATUS: "JOINABLE", MAP: null},
    {NAME: "ROOM4", PLAYERS: 0, P: [], TURN: null, STATUS: "JOINABLE", MAP: null}];


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


io.on('connection', function (socket) {
    socket.on('Iam', function (msg) {
        var player = {ID: socket.id, NAME: msg, ROOM: null, FIELD: 1};
        allPlayer.push(player);
        socket.emit("HI", "Hallo: " + msg + " du hast die ID: " + socket.id);
        socket.emit("DaR00ms", rooms);
    });

    socket.on('JOIN', function (msg) {
        var ro = 0;
        switch (msg) {
            case "ROOM1":
                ro = 0;
                break;
            case "ROOM2":
                ro = 1;
                break;
            case "ROOM3":
                ro = 2;
                break;
            case "ROOM4":
                ro = 3;
                break;
        }
        if (rooms[ro]['PLAYERS'] < 4) {
            Object.keys(allPlayer).forEach(key => {
                if (allPlayer[key]['ID'] === socket.id) {
                    if (allPlayer[key]['ROOM'] !== null) {
                        socket.emit("ERROR", "You are already in a Room!");
                    } else {
                        allPlayer[key]['ROOM'] = "ROOM" + (ro + 1);
                        socket.join("ROOM" + (ro + 1));
                        rooms[ro]['PLAYERS'] += 1;
                        rooms[ro]['P'].push(socket.id);
                        socket.emit("JOINED", "JOINED ROOM" + (ro + 1));
                        if (rooms[ro]['PLAYERS'] > 1) {
                            io.in("ROOM" + (ro + 1)).emit('StartButton', "YES");
                        }
                        if (rooms[ro]['PLAYERS'] === 4) {
                            rooms[ro]['STATUS'] = "FULL";
                        }
                        var players = [];
                        Object.keys(allPlayer).forEach(key => {
                            if (allPlayer[key]['ROOM'] === "ROOM" + (ro + 1)) {
                                players.push(allPlayer[key]);
                            }
                        });
                        io.in("ROOM" + (ro + 1)).emit('AddPlayer', players, null, null);
                        socket.broadcast.emit("DaR00ms", rooms, false);
                    }
                }
            });
        } else {
            socket.emit("ERROR", "ROOM" + (ro + 1) + " is currently Full!");
        }
    });

    socket.on("disconnecting", function () {
        for (var i = allPlayer.length - 1; i >= 0; i--) {
            if (allPlayer[i]['ID'] === socket.id) {
                let room = allPlayer[i]['ROOM'];
                if (room === null) {
                    return;
                }
                allPlayer.splice(i, 1);
                let players = [];
                Object.keys(allPlayer).forEach(key => {
                    if (allPlayer[key]['ROOM'] === room) {
                        players.push(allPlayer[key]);
                    }
                });
                for (var j = rooms[room[4] - 1]['P'].length - 1; i >= 0; i--) {
                    if (rooms[room[4] - 1]['P'][j] === socket.id) {
                        rooms[room[4] - 1]['P'].splice(i, 1);
                        rooms[room[4] - 1]['PLAYERS'] -= 1;
                        break;
                    }
                }
                if (players.length < 2) {
                    rooms[room[4] - 1] = {NAME: room, PLAYERS: 0, P: [], TURN: null, STATUS: "JOINABLE", MAP: null};
                    Object.keys(allPlayer).forEach(key => {
                        if (allPlayer[key]['ROOM'] === room) {
                            allPlayer[key]['ROOM'] = null;
                            allPlayer[key]['FIELD'] = 1;
                        }
                    });
                    io.in(room).emit("DaR00ms", rooms, true);
                    socket.broadcast.emit("DaR00ms", rooms, false);
                    io.in(room).emit("ERROR", "Aborting the game, too few players in Lobby to continue!");
                } else {
                    socket.to(room).emit('AddPlayer', players, null, null);
                }
                break;
            }
        }
    });

    function waitDO(ms, dos) {
        var start = new Date().getTime();
        var end = start;
        while (end < start + ms) {
            end = new Date().getTime();
        }
        dos();
    }

    function wait(ms) {
        var start = new Date().getTime();
        var end = start;
        while (end < start + ms) {
            end = new Date().getTime();
        }
    }

    function sortNumber(a, b) {
        return a - b;
    }

    socket.on("STARTgame", async function () {
        for (var i = allPlayer.length - 1; i >= 0; i--) {
            if (allPlayer[i]['ID'] === socket.id) {
                let room = allPlayer[i]['ROOM'];
                rooms[room[4] - 1]['STATUS'] = 'IN-GAME';
                io.in(room).emit('GameCountDown', "");
                waitDO(5000, () => {
                    io.emit("DaR00ms", rooms, false);
                });
                let mapSize = getRandomInt(25, 45);
                let specialField = [];
                for (var j = 0; j <= getRandomInt(2, (mapSize / 4.5)); j++) {
                    let field = getRandomInt(2, mapSize - 1);
                    while (specialField.includes(field)) {
                        field = getRandomInt(2, mapSize - 1);
                    }
                    specialField.push(field);
                    specialField.sort(sortNumber);
                }
                console.log(specialField);
                rooms[room[4] - 1]['MAP'] = {SIZE: mapSize, FIELDS: specialField};
                io.in(room).emit('START', mapSize);

                let players = [];
                Object.keys(allPlayer).forEach(key => {
                    if (allPlayer[key]['ROOM'] === room) {
                        players.push(allPlayer[key]);
                    }
                });

                io.in(room).emit('AddPlayer', players, rooms[room[4] - 1]['P'][0], mapSize, specialField);
                io.to(rooms[room[4] - 1]['P'][0]).emit('yourTUrn', 'YAYAYA');
                rooms[room[4] - 1]['TURN'] = 0;
                break;
            }
        }
    });


    socket.on('RollDice', function (msg) {
        Object.keys(allPlayer).forEach(async key => {
            if (allPlayer[key]['ID'] === socket.id) {
                let room = allPlayer[key]['ROOM'][4] - 1;
                let playerNum = rooms[room]['TURN'];
                if (rooms[room]['P'][playerNum] === socket.id) {
                    let currField = allPlayer[key]['FIELD'];
                    let dice = getRandomInt(1, 6);
                    let newField = currField + dice;
                    waitDO(1500, () => {
                        io.in(allPlayer[key]['ROOM']).emit("INFO", allPlayer[key]['NAME'] + " got a " + dice)
                    });
                    if (newField > rooms[room]['MAP']['SIZE']) {
                        waitDO(1500, () => {
                            io.in(allPlayer[key]['ROOM']).emit("INFO", allPlayer[key]['NAME'] + "'s number is too big, he got over the finish. More luck next Turn!")
                        });
                    } else if (newField === rooms[room]['MAP']['SIZE']) {
                        waitDO(2000, () => {
                            io.in(allPlayer[key]['ROOM']).emit("WIN", allPlayer[key]['NAME'] + " has won the game!")
                        });
                        rooms[room] = {
                            NAME: allPlayer[key]['ROOM'],
                            PLAYERS: 0,
                            P: [],
                            TURN: null,
                            STATUS: "JOINABLE",
                            MAP: null
                        };
                        let roomNam = allPlayer[key]['ROOM'];
                        Object.keys(allPlayer).forEach(key => {
                            if (allPlayer[key]['ROOM'] === roomNam) {
                                allPlayer[key]['ROOM'] = null;
                                allPlayer[key]['FIELD'] = 1;
                            }
                        });
                        io.in(roomNam).emit("DaR00ms", rooms, true);
                        socket.broadcast.emit("DaR00ms", rooms, false);
                        return;
                    } else {
                        while (rooms[room]['MAP']['FIELDS'].includes(newField)) {
                            waitDO(1500, () => {
                                io.in(allPlayer[key]['ROOM']).emit("INFO", allPlayer[key]['NAME'] + " landed on a special field, what will it be?")
                            });
                            let whatEvent = getRandomInt(1, 2);
                            let fieldSize = getRandomInt(2, 6);
                            if (whatEvent === 1) {
                                waitDO(1500, () => {
                                    io.in(allPlayer[key]['ROOM']).emit("INFO", allPlayer[key]['NAME'] + " got <strong>MINUS " + fieldSize + "</strong> fields, better luck next time!")
                                });
                                if (newField - fieldSize < 0) {
                                    newField = 0;
                                } else {
                                    newField -= fieldSize;
                                }
                            } else {
                                waitDO(1500, () => {
                                    io.in(allPlayer[key]['ROOM']).emit("INFO", allPlayer[key]['NAME'] + " got <strong>PLUS " + fieldSize + "</strong> fields!")
                                });
                                if (newField + fieldSize > rooms[room]['MAP']['SIZE']) {
                                    waitDO(1500, () => {
                                        io.in(allPlayer[key]['ROOM']).emit("INFO", allPlayer[key]['NAME'] + "'s number is too big, he got over the finish. More luck next Turn!")
                                    });
                                } else {
                                    newField += fieldSize;
                                }
                            }
                        }
                        allPlayer[key]['FIELD'] = newField;
                    }
                    waitDO(1500, () => {
                        io.in(allPlayer[key]['ROOM']).emit("INFO", allPlayer[key]['NAME'] + "'s turn has ended!")
                    });
                    if (rooms[room]['TURN'] + 1 === rooms[room]['P'].length) {
                        rooms[room]['TURN'] = 0;
                    } else {
                        rooms[room]['TURN'] += 1;
                    }
                    let players = [];
                    Object.keys(allPlayer).forEach(key => {
                        if (allPlayer[key]['ROOM'] === allPlayer[key]['ROOM']) {
                            players.push(allPlayer[key]);
                        }
                    });
                    io.in(allPlayer[key]['ROOM']).emit('AddPlayer', players, rooms[room]['P'][rooms[room]['TURN']], rooms[room]['MAP']['SIZE'], rooms[room]['MAP']['FIELDS']);
                    waitDO(1500, () => {
                        io.to(rooms[room]['P'][rooms[room]['TURN']]).emit('yourTUrn', 'YAYAYA')
                    });
                }
            }
        });
    })

});


module.exports = app;
